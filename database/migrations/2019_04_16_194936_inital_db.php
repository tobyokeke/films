<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitalDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function(Blueprint $table){
            $table->increments('fid');
            $table->string('name');
            $table->string('slug',191)->unique();
            $table->string('description',5000);
            $table->timestamp('releaseDate');
            $table->enum('rating',[1,2,3,4,5]);
            $table->string('ticketPrice');
            $table->string('country');
            $table->string('photo',2000);
            $table->timestamps();
        });


        Schema::create('comments', function(Blueprint $table){
            $table->increments('cid');
            $table->integer('fid');
            $table->string('name');
            $table->string('comment');
            $table->timestamps();
            });

        Schema::create('genres', function(Blueprint $table){
            $table->increments('gid');
            $table->integer('fid');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('uid');
            $table->string('name');
            $table->string('email',191)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });


        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email',191)->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');

    }
}
