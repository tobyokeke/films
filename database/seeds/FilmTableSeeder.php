<?php

use Illuminate\Database\Seeder;

class FilmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \Illuminate\Support\Facades\DB::table('films')->insert([
            'name' => "JAMES FILM",
            'country' => "USA",
            'slug' => "james-film",
            'photo' => "images/image1.jpg",
            'description' =>"This is description 1",
            'releaseDate' => \Carbon\Carbon::now(),
            'rating' => 2,
            'ticketPrice' => 30,
            'created_at' => '2019-04-17 07:28:25',
            'updated_at' => '2019-04-17 07:28:25',

        ]);

        \Illuminate\Support\Facades\DB::table('films')->insert([
            'name' => "JOHN FILM",
            'country' => "AUSTRALIA",
            'slug' => 'john-film',
            'photo' => "images/image2.jpg",
            'description' =>"This is description 1",
            'releaseDate' => \Carbon\Carbon::now(),
            'rating' => 2,
            'ticketPrice' => 30,
            'created_at' => '2019-04-17 07:28:25',
            'updated_at' => '2019-04-17 07:28:25',

        ]);

        \Illuminate\Support\Facades\DB::table('films')->insert([
            'name' => "MARK FILM",
            'slug' => 'mark-film',
            'country' => "ENGLAND",
            'photo' => "images/image3.jpg",
            'description' =>"This is description 1",
            'releaseDate' => \Carbon\Carbon::now(),
            'rating' => 2,
            'ticketPrice' => 30,
            'created_at' => '2019-04-17 07:28:25',
            'updated_at' => '2019-04-17 07:28:25',
        ]);

        \Illuminate\Support\Facades\DB::table('comments')->insert([
            'fid' => 1,
            'name' => "TOBY",
            'comment' => "This is the comment",
            'created_at' => '2019-04-17 07:28:25',
            'updated_at' => '2019-04-17 07:28:25',

        ]);

        \Illuminate\Support\Facades\DB::table('comments')->insert([
            'fid' => 2,
            'name' => "TOBY",
            'comment' => "This is the comment",
            'created_at' => '2019-04-17 07:28:25',
            'updated_at' => '2019-04-17 07:28:25',
        ]);

        \Illuminate\Support\Facades\DB::table('comments')->insert([
            'fid' => 3,
            'name' => "TOBY",
            'comment' => "This is the comment",
        ]);





    }
}
