<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes();


Route::get('/',function(){
    return redirect('films');
});

//film routes

Route::get('films','FilmsController@index')->name('films.index');
Route::get('films/create','FilmsController@create')->name('films.create');
Route::post('films','FilmsController@store')->name('films.store');
Route::get('films/{slug}','FilmsController@show')->name('films.show');
Route::put('films/{slug}','FilmsController@update')->name('films.update');
Route::delete('films/{slug}','FilmsController@delete')->name('films.delete');


// comment routes


//my convention is to add the method name before the name of the method
// so a post request to add a comment would be 'postAddComment'
// a get request would just be the method name eg 'addComment'


Route::post('add-comment','CommentController@postAddComment');
Route::post('comments','CommentController@comments');
Route::get('logout','FilmsController@logout');



