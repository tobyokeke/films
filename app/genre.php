<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    protected $primaryKey = 'gid';
    protected $table = 'genres';

    public function Film() {
        return $this->belongsTo(film::class,'fid','fid');
    }
}
