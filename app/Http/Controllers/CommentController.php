<?php

namespace App\Http\Controllers;

use App\comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function postAddComment(Request $request) {
        // returning json because i'm consuming this with vuejs

        $comment = new comment();
        $comment->fid = $request->input('fid');
        $comment->name = auth()->user()->name;
        $comment->comment = $request->input('comment');
        $comment->save();
        return response( array( "message" => "Successful", "data" =>  $comment ), 200 );
    }

    public function comments(Request $request) {
        $fid = $request->input('fid');

        $comments = comment::orderBy('created_at','desc')->where('fid',$fid)->get();

        foreach($comments as $comment){
            $comment['time'] = $comment->created_at->diffForHumans();
        }
        return response( array( "message" => "Successful", "data" => $comments  ), 200 );
    }
}
