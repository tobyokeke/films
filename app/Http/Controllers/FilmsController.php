<?php

namespace App\Http\Controllers;

use App\film;
use App\genre;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FilmsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $films = film::orderBy('created_at','desc')->paginate();

        return view('films.manage',[
            'films' => $films
        ]);
    }

    public function create() {
        return view('films.add');
    }

    public function store(Request $request) {
        $name = $request->input('name');
        $releaseDate = $request->input('releaseDate');
        $genres = $request->input('genre');
        $slug = Str::slug($name);


        $releaseDate = Carbon::createFromFormat("m/d/Y H:i",$releaseDate);

        //check if the slug exists and make find a unique one
        while(film::where('slug',$slug)->count() > 0 ){
            $slug = Str::slug($name) . Str::random('2');
        }

        if($request->hasFile('photo')){
            $photo = $request->file('photo');
            $rand = auth()->user()->uid . Str::random(5) . \Carbon\Carbon::now()->timestamp;
            $inputFileName = $photo->getClientOriginalName();

            $filepath = 'uploads/images/' . $rand . $inputFileName;

            Storage::disk('public')->put( $filepath, file_get_contents($photo));

            $photoUrl = asset("storage/" . $filepath);
        } else $photoUrl = url('uploads/images/default.png');

        // create a new film
        $film = new film();
        $film->name = $name;
        $film->slug = $slug;
        $film->photo = $photoUrl;
        $film->country = $request->input('country');
        $film->description = $request->input('description');
        $film->releaseDate = $releaseDate;
        $film->rating = $request->input('rating');
        $film->ticketPrice = $request->input('ticketPrice');
        $film->save();

        foreach($genres as $genre){
            // store all genres for each film
            $newGenre = new genre();
            $newGenre->fid = $film->fid;
            $newGenre->name = $genre;
            $newGenre->save();
        }

        session()->flash('success','Film Created.');
        return redirect()->back();
    }

    public function show($slug) {

        if(film::where('slug',$slug)->count() <= 0 ){
            session()->flash('error','Film does not exist.');
            return redirect()->route('films.index');
        }

        $film = film::where('slug',$slug)->first();

        return view('films.details',[
            'film' => $film
        ]);
    }

    public function update(Request $request, $slug) {
        $name = $request->input('name');
        $releaseDate = $request->input('releaseDate');


        $film = film::where('slug',$slug)->first();
        $film->name = $name;
        $film->slug = str_slug($name);
        $film->description = $request->input('description');
        $film->releaseDate = $releaseDate;
        $film->rating = $request->input('rating');
        $film->ticketPrice = $request->input('ticketPrice');
        $film->save();

        session()->flash('success','Film Updated');
        return redirect()->back();

    }

    public function delete($slug) {
        if(film::where('slug',$slug)->count() <= 0 ){
            session()->flash('error','Film does not exist.');
            return redirect()->route('films.index');
        }

        $film = film::where('slug',$slug)->first();

        $film->delete();

        session()->flash('success','Film Deleted');
        return redirect()->back();

    }

    public function logout() {
        auth()->logout();
        return redirect('/');
    }


}
