<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $primaryKey = 'cid';
    protected $table = 'comments';

    public function Film() {
        return $this->belongsTo(film::class,'fid','fid');
    }
}
