<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    protected $primaryKey = 'fid';
    protected $table = 'films';
    protected $dates = ['releaseDate'];

    public function Genres() {
        return $this->hasMany(genre::class,'fid','fid');
    }

    public function Comments() {
        return $this->hasMany(comment::class,'fid','fid');
    }
}
