@extends('layouts.base')

@section('styles')
    <link rel="stylesheet" href="{{url('css/film-detail.css')}}">
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios@0.18.0/dist/axios.min.js"></script>

@endsection
@section('content')

    <div class="container" id="app" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="row">
            <div class="col-md-12">
                <div class="film-header-section">
                    <div class="chat-film">
                        <h4>
                            <a href="{{url('films')}}">
                                <i class="fas fa-arrow-left"></i>
                            </a>
                            {{$film->name}}</h4>
                    </div>
                </div>
                <div class="film-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row film-gallery">
                                <div class="col-md-8 offset-md-2">
                                    <div class="single-film">
                                        <img src="{{$film->photo}}" class="post-img" alt="film Pics">
                                        <div class="writeup mt-3 pb-3">
                                            <p>{{$film->description}}</p>

                                            <h3>Genres</h3>
                                            @foreach($film->Genres as $genre)
                                            <p>{{$genre->name}}</p>
                                            @endforeach
                                        </div>
                                        <div class="film-info">
                                            <p class="mb-0">{{$film->releaseDate->toFormattedDateString()}}</p>
                                            <p class="mb-0">{{count($film->Comments)}} Comments</p>
                                        </div>
                                    </div>

                                    <div class="comments">
                                        <div class="media mb-3" v-for="comment in comments">
                                            <div class="media-body ">
                                                <div class="comment-heading d-flex">
                                                    <h6 class="mt-0">@{{ comment.name }}</h6>
                                                    <p>@{{comment.time}}</p>
                                                </div>
                                                <p class="mb-0">@{{ comment.comment }}</p>
                                                <div class="comment-actions mt-2">
                                                </div>
                                            </div>
                                        </div>
                                        @if(auth()->check())
                                        <h4>Drop Your Comment</h4>
                                        <form >
                                            <div class="form-row form-group">
                                                <div class="form-group col-md-12">

                                                        <textarea v-model="comment" class="form-control" id="comment" rows="5"
                                                                  placeholder="Your Comment"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-button">
                                                <img v-if="loading" src="{{url('images/loading.gif')}}" style="height: 50px;">
                                                <span v-if="!loading" class="btn btn-primary" id="submit" v-on:click="addComment()">Send Comment</span>
                                            </div>
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


<script>
    Vue.use(axios);
    var app = new Vue({
        el: "#app",
        data: {
            uid : "{{auth()->user()->uid}}",
            fid : "{{$film->fid}}",
            baseUrl : "{{url('/')}}",
            csrftoken : '{{csrf_token()}}',
            comment: null,
            comments : [],
            loading: false
        },
        mounted() {
            this.getComments();
        },
        methods: {

            addComment(){
                let data = {
                    _token : this.csrftoken,
                    uid : this.uid,
                    fid : this.fid,
                    comment: this.comment
                };

                this.loading = true;
                axios.post(this.baseUrl + "/add-comment", data)
                    .then(response => {
                        console.log(response);
                        this.loading = false;

                        if (response.data.message === 'Successful') {

                            this.comment = null;
                            this.getComments();

                        }

                        // JSON responses are automatically parsed.
                    })
                    .catch(e => {
                        this.loading = false;
                        console.log(e);
                    });

            },
            getComments(){
                let data = {
                    fid : this.fid,
                };

                axios.post( this.baseUrl + "/comments", data)
                    .then(response => {
                        this.loading = false;

                        if (response.data.message === 'Successful') {

                            console.log(response.data.data);
                            this.comments = response.data.data;
                            this.comment = null;

                        }

                        // JSON responses are automatically parsed.
                    })
                    .catch(e => {
                        this.loading = false;
                        console.log(e.response.data);
                        console.log(e.response.data.message);
                    });

            }

        }
    });
</script>

@endsection
