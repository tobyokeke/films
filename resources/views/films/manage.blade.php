
@extends('layouts.base')

@section('styles')
    <link rel="stylesheet" href="{{url('css/films.css')}}">
@endsection

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="film-header-section">
                    <div class="chat-film">
                        <h4>ALL FILMS</h4>
                    </div>
                    <div class="film-header">
                        <ul class="nav">
                        </ul>
                    </div>
                </div>
                <div class="film-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row film-gallery">

                                @foreach($films as $film)
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <a href="{{route('films.show',['slug' => $film->slug ])}}">
                                        <div class="single-film">
                                            <img src="{{$film->photo}}" alt="film Pics">
                                            <h5>{{$film->name}}</h5>
                                            <div class="film-info">
                                                <p class="mb-0">{{$film->releaseDate->toFormattedDateString()}}</p>
                                                <p class="mb-0">{{count($film->Comments)}} Comments</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                @endforeach

                                @if(count($films) <= 0)
                                    No Films Yet
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

