@extends('layouts.base')

@section('styles')

    <link rel="stylesheet" type="text/css" href="{{url('css/add-film.css')}}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>

@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="profile-header-section">
                    <div class="chat-profiler">
                        <h4>Add Film</h4>
                    </div>
                    <div class="profile-header">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link">Add Film</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="profile-body">
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="profile-form-fields">
                                <form method="post" enctype="multipart/form-data" action="{{route('films.store')}}">
                                    @csrf
                                    <input  onchange="readURL(this);" type="file" style="display: none;" id="hiddenFile1"  name="photo">

                                    <div class="form-row form-group">
                                        <div class="form-group col-md-6">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" id="name" name="name" required placeholder="Enter Name">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="country">Country</label>
                                            <input type="text" class="form-control" id="country" name="country" required placeholder="Enter Country">
                                        </div>
                                    </div>
                                    <div class="contact-details-section">
                                        <div class="form-row form-group">
                                            <div class="form-group col-md-12">
                                                <label for="description">Description</label>
                                                <textarea class="form-control" id="description" required name="description" placeholder="Enter Description">

                                                </textarea>
                                            </div>
                                        </div>

                                        <div class="form-row form-group">
                                            <div class="form-group col-md-2">
                                                <label for="City">Rating</label>
                                                <select name="rating" required>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label for="releaseDate">Release Date</label>
                                                <input type="text" class="form-control" id="releaseDate" required name="releaseDate" placeholder="Enter Release Date">
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label for="ticketPrice">Ticket Price</label>
                                                <input type="text" class="form-control" id="ticketPrice" required name="ticketPrice" placeholder="Enter Ticket Price">
                                            </div>
                                        </div>
                                        <div class="row" id="genre">
                                            <div class="form-group col-md-10">
                                                <label for="genre">Genre</label>
                                                <input type="text" class="form-control" id="genre" required name="genre[]" placeholder="Enter Genre">
                                            </div>
                                            <div class="col-2">
                                                <button type="button" class="btn btn-primary"  id="addGenre">Add Genre</button>
                                            </div>

                                        </div>



                                    </div>

                                    <button class="btn btn-primary">Submit</button>

                                </form>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-12"></div>
                        <div class="col-md-3 col-sm-12">

                            <div id="preview" class="row profile-gallery">

                            </div>

                            <div class="row">
                                <div class="upload-btn col-md-12">
                                    <a id="addImageButton" class="btn btn-sm btn-secondary">Upload Photo</a><br>
                                    <small>Click on a photo to remove it.</small>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready( function() {

            $('#addImageButton').on('click', function () {
                $('#hiddenFile1').click();
            });

            $('#addGenre').on('click',function(){
                $('#genre').append('<div class="form-group col-md-10">' +
                    '<label for="genre">Genre</label>' +
                    '<input type="text" class="form-control" id="genre" required name="genre[]" placeholder="Enter Genre">\n' +
                    '</div>')
            });

            $('#releaseDate').datetimepicker();

        });
    </script>


    <script>

        function deleteItem(item){
            console.log(item);
        }

        function hover(item){

            console.log('hovered');
            $(item).css('background-color','red');
            $(item).children('img').css('opacity',0.5);
            console.log($(item).children('img'));


        }

        function removeHover(item){
            console.log('cleared');
            $(item).css('background-color','transparent');
            $(item).children('img').css('opacity',1);
        }

        function removeItem(item){
            console.log('removed');
            $(item).remove();
        }

        function readURL(input) {

            for(i=0; i< input.files.length; i++) {


                if (input.files && input.files[i]) {


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').append('<div class="col-4 image" onclick="removeItem(this)" onmouseleave="removeHover(this)"  onmouseover="hover(this)"> <img src ="' + e.target.result + '" alt="Profile Pics" ></div>');
                    };


                    reader.readAsDataURL(input.files[i]);
                }
            }
        }


    </script>
@endsection
