<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Films App</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('fontAwesome/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{url('css/jquery-ui.min.css')}}">

    <script src="{{url('js/jquery.min.js')}}" ></script>

    <script type="text/javascript" src="{{url('js/jquery-ui.min.js')}}"></script>

    @yield('styles')
</head>

<body>
<div class="container-fluid navigation-background">
    <div class="row">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="{{url('/')}}">
                    FILMS APP
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler"
                        aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarToggler">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('films.create')}}">Add Film</a>
                        </li>

                        <li class="nav-item d-flex">
                            <div class="user-pics">

                                <div class="dropdown">
                                    <button class="dropbtn"><a
                                                class="nav-link user-profile-name">Logged in as {{auth()->user()->name}}</a></button>
                                    <div class="arrow_div"></div>
                                    <div class="dropdown-content">
                                        <a href="{{route('films.create')}}"><i class="far fa-plus"></i> Add Film</a>
                                        <a href="{{route('films.index')}}"><i class="far fa-video"></i> Films</a>
                                        <a href="{{url('logout')}}"><i class="far fa-sign-out rotate-signout"></i> Logout</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="content-body">

    @include('notification')
    @yield('content')

    <div class="copyright">
        <div class="container">
            <p>(C) 2018. FILM APP - Developed for codeline.</p>
            <ul>
                <li>Terms of Use</li> |
                <li>Privacy Policy</li> |
                <li>Contact Us</li>
            </ul>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
</body>

</html>
