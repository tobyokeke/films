<?php use Illuminate\Support\Facades\Session; ?>
<div>
    @if(Session::has('success'))
        <div id="notification" class="alert alert-success" style="margin-bottom: 5px;margin-right: 60px;margin-left: 60px;" align="center">{{Session::get('success')}}</div>
    @endif

    @if(Session::has('error'))
        <div id="notification" class="alert alert-danger" style="margin-bottom: 5px;margin-right: 60px;margin-left: 60px;" align="center">{{Session::get('error')}}</div>
    @endif

    <script>
        $(document).ready(function () {
            var notification = $('#notification');

            setTimeout(function () {
                notification.addClass('fade').css('display','none');
            },10000);
        })
    </script>

</div>
